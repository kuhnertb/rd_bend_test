#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 19:25:14 2016
@author: mcanulty
"""

import threading
import time, sys
import queue
import mido
import mido.backends.rtmidi  
import signal

# Import needed modules from osc4py3
from osc4py3.as_eventloop import *
from osc4py3 import oscbuildparse

mido.set_backend('mido.backends.rtmidi')

# Start the system.
osc_startup()

# Make client channels to send packets.
osc_udp_client('127.0.0.1', 5050, "max")


sensor_array = []

class Force_Glove_Demo_Thread(threading.Thread):
    """ 
        Thread:
        received midi from force glove demo, converts into an indexed numpy array
    """


    #*****************
    #***  Init  ******
    #*****************
    def __init__ (self, device_name="Force_Glove"):
        threading.Thread.__init__(self)
        global sensor_array
        self.arr  = sensor_array
        self.device_name = device_name
        self.connected = False


    #**********************************
    #***  return pointer to array  ****
    #**********************************
    def return_pointer_to_array(self):
        return self.arr


    #*****************
    #***  Close  *****
    #*****************        
    def close(self):
        self.running = False
    
    #*****************
    #***  Run ********
    #*****************     
    def run(self):
        self.running = True
        inputs = mido.get_input_names()

        print("\n\n\n" , "Checking midi ports...")
        print("Available inputs are:", inputs)



        if self.device_name in inputs:
            midiLoop = True

            print("\n", self.device_name, " available, opening port...")
            sys.stdout.flush()              # Have to flush stdout here, or else
                                            # the output gets held up until later
            inport = mido.open_input(self.device_name)
            self.connected = True
        else:
            midiLoop = False
            inport = None

        #**************************
        #**** MAIN THREAD LOOP  ***
        #**************************
        x = 0
        y = 0
        adc = 0
        
        
        while midiLoop is True:

            time.sleep(0.1)
            for msg in inport.iter_pending():  #without iter_pending, this blocks like a while loop
                
                if(msg.channel == 15):
                    senseNum = msg.value 
                    driveNum = msg.control                
                else:
                    adc = (     ((msg.channel & 0x7F) << 14)
                            |   ((msg.control & 0x7F) << 7)
                            |    (msg.value & 0x7F))
                    
                    #sensor_name = str(senseNum) + '_' + str(driveNum)
                    
                    #try:
                        #sensor_array_num = dict_of_sensor_name_to_index[sensor_name]
                    #except:
                        #pass
                        ##print("err:", sensor_name, adc)
                    #else:
                        #self.arr[sensor_array_num] = adc
                    
                    #print(sensor_name, adc)
                    

                    # Build a simple message and send it.
                    msg = oscbuildparse.OSCMessage('/midisensor', ",iii", [senseNum, driveNum, adc])
                    osc_send(msg, "max")
                    osc_process()
                    
            

            if self.running is False:
                midiLoop = False
        #**************************
        #**** CLEANUP  ************
        #**************************
        osc_terminate()
        if inport != None:
            inport.close()
            print(self.device_name + ': Midi port closed')
        else:
            print("No device to close")

#==============================================================================
#==============================================================================








midi_thread = None    


def main():



    global running

    force_glove_thread = Force_Glove_Demo_Thread()
    force_glove_thread.start()

    data_array =  force_glove_thread.return_pointer_to_array()
    count = 0
    while running:
        time.sleep(1)
        #print(data_array)
            
    force_glove_thread.close()

    time.sleep(1)

running = False

if __name__ == '__main__':
    
    
    #==============================================================================        
    def signal_handler(signal, frame):      
        global running                  # This catches Ctrl+C  and allows the program to properly      
        running = False                 # close itself, ideally preventing any attached devices 
                                        # from being unintentionally left open and inaccessible.
        print("SIGINT")
    
    if __name__ == '__main__':
        signal.signal(signal.SIGINT, signal_handler)
    #==============================================================================
    running = True
    
    main()

