#!/usr/bin/env python
#coding:utf-8

from bebop.sensors.bebop_sensor_device import Bebop_Sensor_Device_Descriptor
from midiSensor.sensor_define_generators import alicat_csv_titles_with_metric, gen_sensor_names_with_sense_drive

#array_of_indexed_sensor_names = ['s0_d0', 's0_d1', 's0_d2', 's0_d3',\
#'s0_d4', 's0_d5', 's0_d6', 's0_d7', 's0_d11', 's0_d12', 's0_d13', 's1_d0',\
#'s1_d1', 's1_d2', 's1_d3', 's1_d4', 's1_d5', 's1_d6', 's1_d7', 's1_d8',\
#'s1_d9', 's1_d10', 's1_d11', 's1_d12', 's1_d13']
Left_Wing = ['s5_d0', 's6_d0', 's7_d0', 's8_d0', 's9_d0', 's5_d1', 's6_d1', 's7_d1', 's8_d1', 's9_d1',  's5_d2', 's6_d2', 's7_d2', 's8_d2', 's9_d2',  's5_d3', 's6_d3', 's7_d3', 's8_d3', 's9_d3',  's5_d4', 's6_d4', 's7_d4', 's8_d4', 's9_d4', 's5_d5', 's6_d5', 's7_d5', 's8_d5', 's9_d5',  's5_d6', 's6_d6', 's7_d6', 's8_d6', 's9_d6',  's5_d7', 's6_d7', 's7_d7', 's8_d7', 's9_d7']
Right_Wing = ['s0_d0', 's1_d0', 's2_d0', 's3_d0', 's4_d0','s0_d1', 's1_d1', 's2_d1', 's3_d1', 's4_d1','s0_d2', 's1_d2', 's2_d2', 's3_d2', 's4_d2','s0_d3', 's1_d3', 's2_d3', 's3_d3', 's4_d3','s0_d4', 's1_d4', 's2_d4', 's3_d4', 's4_d4','s0_d5', 's1_d5', 's2_d5', 's3_d5', 's4_d5','s0_d6', 's1_d6', 's2_d6', 's3_d6', 's4_d6','s0_d7', 's1_d7', 's2_d7', 's3_d7', 's4_d7']

array_of_indexed_sensor_names = Right_Wing + Left_Wing #['s0_d0', 's1_d0', 's2_d0', 's3_d0', 's4_d0', 's5_d0', 's6_d0', 's7_d0', 's8_d0', 's9_d0', 's0_d1', 's1_d1', 's2_d1', 's3_d1', 's4_d1', 's5_d1', 's6_d1', 's7_d1', 's8_d1', 's9_d1', 's0_d2', 's1_d2', 's2_d2', 's3_d2', 's4_d2', 's5_d2', 's6_d2', 's7_d2', 's8_d2', 's9_d2', 's0_d3', 's1_d3', 's2_d3', 's3_d3', 's4_d3', 's5_d3', 's6_d3', 's7_d3', 's8_d3', 's9_d3', 's0_d4', 's1_d4', 's2_d4', 's3_d4', 's4_d4', 's5_d4', 's6_d4', 's7_d4', 's8_d4', 's9_d4', 's0_d5', 's1_d5', 's2_d5', 's3_d5', 's4_d5', 's5_d5', 's6_d5', 's7_d5', 's8_d5', 's9_d5', 's0_d6', 's1_d6', 's2_d6', 's3_d6', 's4_d6', 's5_d6', 's6_d6', 's7_d6', 's8_d6', 's9_d6', 's0_d7', 's1_d7', 's2_d7', 's3_d7', 's4_d7', 's5_d7', 's6_d7', 's7_d7', 's8_d7', 's9_d7'] #gen_sensor_names_with_sense_drive(0, 9, 0, 7)
num_sensors = len(array_of_indexed_sensor_names)

ShoulderPad_device_dict = {
    'midi_device_name': 'Shoulder_Pad', 
    'num_sensors': num_sensors,
    'array_of_indexed_sensor_names': array_of_indexed_sensor_names, 
    'leftmost_csv_header_string': alicat_csv_titles_with_metric, 
    'device_type' : 'bebop_midi_device', 
    'data_encoding' : 'bebop_midi_xy_int',
    'right_wing' : Right_Wing,
    'left_wing' : Left_Wing
}


ShoulderPad_descriptor = Bebop_Sensor_Device_Descriptor(ShoulderPad_device_dict)

device_descriptor = ShoulderPad_descriptor

if __name__ == "__main__":

    print(ShoulerPad_descriptor.csv_header, end='')
    print(ShoulerPad_descriptor.first_sensor_column_in_csv)
    #print(array_of_indexed_sensor_names)
    #print(alicat_csv_titles_with_metric.split(sep=','))
    #print(first_sensor_position)
    
    print(ShoulerPad_descriptor.dict_of_sensor_name_to_index)
    
    print(array_of_indexed_sensor_names)
    
    
