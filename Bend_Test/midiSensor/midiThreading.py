#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 19:25:14 2016
@author: mcanulty
"""

from .midiquery import MidiQuery, checkForMidi
from .units import kgcm2_to_psi, area_of_circle
#import copy
from . import calib
import time
import queue
import signal
from . import graphics
from .graphicsHelpers import numberBox

#==============================================================================
#==============================================================================

running = True

def signal_handler(signal, frame):      
    global running                  # This catches Ctrl+C  and allows the program to properly      
    running = False                 # close itself, ideally preventing any attached devices 
                    # from being unintentionally left open and inaccessible.
    print("SIGINT")

signal.signal(signal.SIGINT, signal_handler)


#==============================================================================
#==============================================================================





def sum_dict(d):
    totalSum = 0
    for key in d:
        totalSum += d[key]
    return totalSum



#==============================================================================
#==============================================================================
deviceName = 'SeatSensor_mini'

NUM_SENSORS     = 10
SENSOR_DIAMETER = 1.3    #centimeters


SINGLE_SENSOR_AREA = area_of_circle(SENSOR_DIAMETER/2)
TOTAL_SENSOR_AREA  = SINGLE_SENSOR_AREA * NUM_SENSORS



SCREEN_X =  700
SCREEN_Y =  500      
screen = graphics.GraphWin("Data Readout", SCREEN_X, SCREEN_Y, autoflush=False)

def Main():

    global running

    #***************************************
    #****  SET UP DISPLAY OUTPUT  **********
    #***************************************  

    totalForceDisp = numberBox(screen, 20, SCREEN_Y - 40, "totalForce")
    adcBox =    []
    kgBox  =    []
    psiBox =    []
    kgcm2Box =  []

    for num in range(11):
        adcBox.append(  numberBox(screen,  50, 40 + (32 * num), "adc" + str(num)))
        kgBox.append(   numberBox(screen, 200, 40 + (32 * num), "kg" + str(num)))
        psiBox.append(  numberBox(screen, 350, 40 + (32 * num), "PSI_" + str(num)))
        kgcm2Box.append(numberBox(screen, 500, 40 + (32 * num), "kgcm2_" + str(num)))



    graphics.update()


    #***************************************
    #****  SET UP QUEUES AND THREADS  ******
    #***************************************    
    midiQueue = queue.Queue()
    midi_thread = MidiQuery(midiQueue, deviceName)
    midi_thread.start()

    #***************************************
    #****  MAIN LOOP  **********************
    #*************************************** 
    forceDict = {}
    kgcm2Dict = {}
    psiDict   = {}
    adcDict   = {}
    mainRunning = True
    seconds = time.time()
    print("entering main")
    while mainRunning is True:


        #**************************
        #****  I/O UPDATE  ********
        #**************************
        midiDict  = checkForMidi(midiQueue)

        if len(midiDict) > 0:

            #***  FIND UNIQUE SENSOR NUMBER  ***
            key = calib.arrayMap[midiDict['x']][midiDict['y']]


            #***  GET SENSOR_ADC_DATA  ***
            adc = midiDict['adc']

            #***  LOOK UP CORRESPONDING FORCE VALUE FOR ADC READING
            try:
                thisForce = calib.kg[key][adc]
            except IndexError:
                print(("Index Error on sensor:", key, "with adc val:", adc))
                thisForce = 0

            #***  CALC AND CONVERT TO PRESSURE VALUES

            thisKgcm2  = thisForce / SINGLE_SENSOR_AREA
            thisPsi    = kgcm2_to_psi(thisKgcm2)

            #***  SAVE DATA 
            adcDict[key]    = adc
            forceDict[key]  = thisForce
            kgcm2Dict[key]  = thisKgcm2
            psiDict[key]    = thisPsi

            #totalForce = sum_dict(sensorDict)
            #print "total force", totalForce

            #***  UPDATE NUMBER BOXES
            adcBox[key].setText(    '%4.0f' % (adcDict[key]))
            kgBox[key].setText(     '%4.2f' % (forceDict[key]))
            kgcm2Box[key].setText(  '%4.2f' % (kgcm2Dict[key]))
            psiBox[key].setText(    '%4.2f' % (psiDict[key]))           

        #**************************
        #****  DISPLAY UPDATE  ****
        #**************************
        if time.time() - seconds > 0.1:
            seconds = time.time()
            totalForceDisp.setText(str(sum_dict(forceDict)))
            graphics.update()



        #**************************
        #****  CONTROLLED EXIT  ***
        #**************************   
        if screen.closed is True:
            running = False

        if running is False:
            midi_thread.close()
            screen.close() 
            time.sleep(1)
            mainRunning = False



    print("finished")  



#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================

if __name__ is '__main__':


    Main()

