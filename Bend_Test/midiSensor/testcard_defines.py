#!/usr/bin/env python
#coding:utf-8

from midiSensor.sensor_define_generators import gen_sensor_names, gen_sensor_names_with_sense_drive, create_csv_titles, create_sensor_dict, alicat_csv_titles_with_metric




test_num_sensors = 4
num_sense = 2
num_drive = 2
end_sense = 0
end_drive = 3

midi_device_name = 'SeatSensor'


def run_code_generators():
    
    global array_of_indexed_sensor_names,  array_of_indexed_adc_sensor_names
    global dict_of_sensor_name_to_index,  dict_of_adc_sensor_name_to_index
    global num_sensors, csv_header, first_sensor_position
    
    
    array_of_indexed_adc_sensor_names = gen_sensor_names_with_sense_drive(0, end_sense, 0, end_drive, prefix='adc_')       
    
    array_of_indexed_sensor_names = ['s0_d0', 's0_d1', 's0_d2', 's0_d3']
    
    dict_of_adc_sensor_name_to_index = create_sensor_dict(array_of_indexed_adc_sensor_names)
    
    array_of_indexed_sensor_names = gen_sensor_names_with_sense_drive(0, end_sense, 0, end_drive, prefix='')       
    dict_of_sensor_name_to_index = create_sensor_dict(array_of_indexed_sensor_names)
    
    csv_header = alicat_csv_titles_with_metric + create_csv_titles(array_of_indexed_sensor_names)    
    first_sensor_position = len(alicat_csv_titles_with_metric.split(sep=',')) - 1
    
    num_sensors = len(array_of_indexed_sensor_names)    
    
    
    #print('num_sensors is', num_sensors)
    assert num_sensors == test_num_sensors
    assert num_sensors == len(array_of_indexed_adc_sensor_names)
    assert num_sensors == len(dict_of_adc_sensor_name_to_index)
    assert num_sensors == len(array_of_indexed_sensor_names)
    assert num_sensors == len(dict_of_sensor_name_to_index)
    
    #print(csv_header)
    


    


run_code_generators()


if __name__ == "__main__":
    print(csv_header, end='')
    print(array_of_indexed_sensor_names)
    print(num_sensors)
    print(alicat_csv_titles_with_metric.split(sep=','))
    print(first_sensor_position)
    
    
