#!/usr/bin/env python
#coding:utf-8

from bebop.sensors.bebop_sensor_device import Bebop_Sensor_Device_Descriptor
from midiSensor.sensor_define_generators import alicat_csv_titles_with_metric, gen_sensor_names_with_sense_drive


array_of_indexed_sensor_names = gen_sensor_names_with_sense_drive(0,61, 0,0)
num_sensors = len(array_of_indexed_sensor_names)


football_device_dict = {
    'midi_device_name': 'football', 
    'num_sensors': num_sensors,
    'array_of_indexed_sensor_names': array_of_indexed_sensor_names, 
    'leftmost_csv_header_string': alicat_csv_titles_with_metric
}


Football_descriptor = Bebop_Sensor_Device_Descriptor(football_device_dict)

device_descriptor = Football_descriptor

if __name__ == "__main__":

    print(Football_descriptor.csv_header, end='')
    #print(array_of_indexed_sensor_names)
    print(Football_descriptor.first_sensor_column_in_csv)
    #print(alicat_csv_titles_with_metric.split(sep=','))
    #print(first_sensor_position)
    
    print(Football_descriptor.dict_of_sensor_name_to_index)
    
    
