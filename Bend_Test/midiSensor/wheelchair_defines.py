#!/usr/bin/env python
#coding:utf-8

from midiSensor.sensor_define_generators import gen_sensor_names, gen_sensor_names_with_sense_drive, create_csv_titles, create_sensor_dict, alicat_csv_titles_with_metric

'''
This file contains various predefined names and indexes
in order to help with writing CSV files in consistent orders. 

'''




test_num_sensors = (10 * 12)
num_sense = 12
num_drive = 10
end_sense = num_sense - 1
end_drive = num_drive - 1

def run_code_generators():
    
    global array_of_indexed_sensor_names,  array_of_indexed_adc_sensor_names
    global dict_of_sensor_name_to_index,  dict_of_adc_sensor_name_to_index
    global num_sensors, csv_header
    
    
    array_of_indexed_adc_sensor_names = gen_sensor_names_with_sense_drive(0, end_sense, 0, end_drive, prefix='adc_')       
    dict_of_adc_sensor_name_to_index = create_sensor_dict(array_of_indexed_adc_sensor_names)
    
    array_of_indexed_sensor_names = gen_sensor_names_with_sense_drive(0, end_sense, 0, end_drive, prefix='')       
    dict_of_sensor_name_to_index = create_sensor_dict(array_of_indexed_sensor_names)
    
    csv_header = alicat_csv_titles_with_metric + create_csv_titles(array_of_indexed_sensor_names)    
    
    
    num_sensors = len(array_of_indexed_sensor_names)    
    
    
    #print('num_sensors is', num_sensors)
    assert num_sensors == test_num_sensors
    assert num_sensors == len(array_of_indexed_adc_sensor_names)
    assert num_sensors == len(dict_of_adc_sensor_name_to_index)
    assert num_sensors == len(array_of_indexed_sensor_names)
    assert num_sensors == len(dict_of_sensor_name_to_index)
    
    #print(csv_header)
    


    


run_code_generators()

    
