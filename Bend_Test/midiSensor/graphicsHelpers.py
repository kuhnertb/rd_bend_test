#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 15:19:46 2016
@author: mcanulty
"""

from . import graphics
import time




          
        
class numberBox:
    #*****************
    #***  Init  ******
    #*****************
    
    
    
    def __init__ (self, canvas, x, y, name):
        
        letterWidth = 10
        
        self.name = name
        xOffset = len(name) * letterWidth
        
        
        self.nameTxt = graphics.Text(graphics.Point(x,y), name)
        self.nameTxt.setFace('Source Code Pro')
        self.nameTxt.setSize(14)
        self.nameTxt.setStyle('bold')
        self.nameTxt.draw(canvas)
        
        self.numTxt = graphics.Text(graphics.Point(x+xOffset, y), "000000")
        self.numTxt.setFace('Source Code Pro')
        self.numTxt.setSize(14)
        self.numTxt.draw(canvas)
        
        self.setText("000000")
        
        
        xOffsetBox = len(name + "000000") * 9
        yOffsetBox = 20
        
       
        
        self.box = graphics.Rectangle(graphics.Point(x-1, y-1), graphics.Point(x+xOffsetBox, y+yOffsetBox))
        self.box.draw(canvas)
        
    def setText(self,text):
        self.numTxt.setText(text)

        
        
if __name__ is "__main__":
    g = graphics
    screen = g.GraphWin("testDisp", 500, 500, autoflush=True)
    
    boxes = []
    for i in range(10):
        boxes.append(numberBox(screen, 200, 100 + (32 * i), "PSI_" + str(i)))
    
    
    
    
    for i in range(20):
        time.sleep(0.5)
        for box in boxes:
            box.setText("0000" + str(i))
    
        
    
    screen.close()    # Close window when done
