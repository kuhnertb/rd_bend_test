#!/usr/bin/env python
#coding:utf-8

from midiSensor.sensor_define_generators import gen_sensor_names, gen_sensor_names_with_sense_drive, create_csv_titles, create_sensor_dict, alicat_csv_titles

'''
This file contains various predefined force_glove_demo names and indexes
in order to help with writing CSV files in consistent orders.

'''

# mark10_csv_header = "seconds,Plunger_Area,Mark10_Force,Calc_Pressure,s0_d0,s0_d1,s0_d2,s0_d3,s1_d0,s1_d1,s1_d2,s1_d3,s2_d0,s2_d1,s2_d2,s2_d3,s3_d0,s3_d1,s3_d2,s3_d3\n"

test_num_sensors = 16

def run_code_generators():

    global array_of_indexed_sensor_names,  array_of_indexed_adc_sensor_names
    global dict_of_sensor_name_to_index,  dict_of_adc_sensor_name_to_index
    global csv_header
    global num_sensors
    global print

    array_of_indexed_adc_sensor_names = gen_sensor_names_with_sense_drive(0, 3, 0, 3, prefix='adc_')
    dict_of_adc_sensor_name_to_index = create_sensor_dict(array_of_indexed_adc_sensor_names)


    array_of_indexed_sensor_names = gen_sensor_names_with_sense_drive(0, 3, 0, 3, prefix='')
    dict_of_sensor_name_to_index = create_sensor_dict(array_of_indexed_sensor_names)

    csv_header = alicat_csv_titles + create_csv_titles(array_of_indexed_sensor_names)


    num_sensors = len(array_of_indexed_sensor_names)


    #print('num_sensors is', num_sensors)
    assert num_sensors == test_num_sensors
    assert num_sensors == len(array_of_indexed_adc_sensor_names)
    assert num_sensors == len(dict_of_adc_sensor_name_to_index)
    assert num_sensors == len(array_of_indexed_sensor_names)
    assert num_sensors == len(dict_of_sensor_name_to_index)

    #print(csv_header)

run_code_generators()


if __name__ == "__main__":

    print(dict_of_sensor_name_to_index)
