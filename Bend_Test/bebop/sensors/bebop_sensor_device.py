#!/usr/bin/env python
#coding:utf-8

from midiSensor.sensor_define_generators import gen_sensor_names_with_sense_drive, create_csv_titles, create_sensor_dict, alicat_csv_titles_with_metric


base_device_dict = {
    'midi_device_name': 'unknown device', 
    'num_sensors': 0,
    'array_of_indexed_sensor_names': [], 
    'leftmost_csv_header_string': ''
}


class Bebop_Sensor_Device_Descriptor(object):
    
    '''
    Bebop_Sensor_Device_Descriptor takes a small dictionary and builds
    a consistent device descriptor for use in other programs
    
    To be implemented:
        x/y sensor location for generic sensor display design
        sensor group definition and lookup (like for force glove)
        
    '''
    
    def __init__(self, device_dict=base_device_dict, **kwargs):
        
        #check for any override parameters
        for a in kwargs:
            device_dict[a] = kwargs[a]
        
        self.device_name = device_dict['midi_device_name']
        self.asserted_num_sensors = device_dict['num_sensors']
        self.array_of_indexed_sensor_names = device_dict['array_of_indexed_sensor_names']
        self.leftmost_csv_header_string = device_dict['leftmost_csv_header_string']
        try:
            self.device_type = device_dict['device_type']
            self.data_encoding = device_dict['data_encoding']
        except:
            print("no device type specified, defaulting to standard midi device")
            self.device_type = 'bebop_midi_device'
            self.data_encoding = 'bebop_midi_xy_int'
            
        
        self.init_properties()
    
    
    def init_properties(self):
        self.first_sensor_column_in_csv = len(self.leftmost_csv_header_string.split(sep=',')) - 1 
        self.dict_of_sensor_name_to_index = create_sensor_dict(self.array_of_indexed_sensor_names)
        self.csv_header = self.leftmost_csv_header_string + create_csv_titles(self.array_of_indexed_sensor_names)
        self.num_sensors = len(self.array_of_indexed_sensor_names)
        
        if (self.num_sensors != self.asserted_num_sensors or
            self.num_sensors != len(self.array_of_indexed_sensor_names) or
            self.num_sensors != len(self.dict_of_sensor_name_to_index)):
            
            print('Inconsistency in', self.device_name, 'properties')
            print('num_sensors does not match initial description' )
            print('asserted num sensors', self.asserted_num_sensors)
            print('given num sensors', self.num_sensors)
            
        
        self.print_self_description()
        
    
    def change_leftmost_csv_headers(self, header_string):
        self.leftmost_csv_header_string = header_string
        self.init_properties()

    def change_sensor_names(self, array_of_indexed_sensor_names):
        self.array_of_indexed_sensor_names =  array_of_indexed_sensor_names
        self.init_properties()
        

    def print_self_description(self):
        print("Midi device name:", self.device_name)
        print("num sensors:", self.num_sensors)




if __name__ == "__main__":
#    test_device = Bebop_Sensor_Device_Descriptor(num_sensors=1)
    test_device = Bebop_Sensor_Device_Descriptor()