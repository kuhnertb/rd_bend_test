#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 19:25:14 2016
@author: mcanulty
"""

import threading
import time, sys
import queue
import mido
import mido.backends.rtmidi  
import signal
import numpy as np
from midiSensor.wheelchair_defines import dict_of_sensor_name_to_index, num_sensors

mido.set_backend('mido.backends.rtmidi')




sensor_array = np.zeros(num_sensors, "uint16")

class MIDI_Data_Monitor_Thread(threading.Thread):
    """ 
        
    """


    #*****************
    #***  Init  ******
    #*****************
    def __init__ (self, device_name=None):
        threading.Thread.__init__(self)
        global sensor_array
        self.arr  = sensor_array
        self.given_name = device_name
        self.connected = False


    #**********************************
    #***  return pointer to array  ****
    #**********************************
    def return_pointer_to_array(self):
        return self.arr


    #*****************
    #***  Close  *****
    #*****************        
    def close(self):
        self.running = False
    
    #*****************
    #***  Run ********
    #*****************     
    def run(self):
        self.running = True
        inputs = mido.get_input_names()

        print("\n\n\n" , "Checking midi ports...")
        print("Available inputs are:", inputs)
        
        
        #added in for compatibility with windows.  Finds first matching midi device name
        self.device_name = next((string for string in inputs if self.given_name in string), None)
        

        if self.device_name in inputs:
            midiLoop = True

            print("\n", self.device_name, " available, opening port...")
            sys.stdout.flush()              # Have to flush stdout here, or else
                                            # the output gets held up until later
            inport = mido.open_input(self.device_name)
            self.connected = True
        else:
            midiLoop = False
            inport = None

        #**************************
        #**** MAIN THREAD LOOP  ***
        #**************************
        x = 0
        y = 0
        adc = 0
        
        
        while midiLoop is True:

            time.sleep(0.1)
            for msg in inport.iter_pending():  #without iter_pending, this blocks like a while loop
                
                if(msg.channel == 15):
                    senseNum = msg.value 
                    driveNum = msg.control                
                else:
                    adc = (     ((msg.channel & 0x7F) << 14)
                            |   ((msg.control & 0x7F) << 7)
                            |    (msg.value & 0x7F))
                    
                    sensor_name = 's' + str(senseNum) + '_' + 'd' + str(driveNum)
                    
                    try:
                        sensor_array_num = dict_of_sensor_name_to_index[sensor_name]
                    except:
                        pass
                        print("err:", sensor_name, adc)
                    else:
                        self.arr[sensor_array_num] = adc
                    
                    #print(sensor_name, adc)

            if self.running is False:
                midiLoop = False
        #**************************
        #**** CLEANUP  ************
        #**************************
        if inport != None:
            inport.close()
            print(self.device_name + ': Midi port closed')
        else:
            print("No device to close")

#==============================================================================
#==============================================================================








midi_thread = None    


def main():



    global running

    wheelchair_thread = MIDI_Data_Monitor_Thread("Wheelchair_Sensor")
    wheelchair_thread.start()

    data_array =  wheelchair_thread.return_pointer_to_array()
    count = 0
    while running:
        time.sleep(1)
        print(data_array)
            
    midi_thread.close()

    time.sleep(1)

running = False

if __name__ == '__main__':
    
    
    #==============================================================================        
    def signal_handler(signal, frame):      
        global running                  # This catches Ctrl+C  and allows the program to properly      
        running = False                 # close itself, ideally preventing any attached devices 
                                        # from being unintentionally left open and inaccessible.
        print("SIGINT")
    
    if __name__ == '__main__':
        signal.signal(signal.SIGINT, signal_handler)
    #==============================================================================
    running = True
    
    main()

