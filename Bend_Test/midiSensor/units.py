#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 19:02:30 2016
@author: mcanulty
"""

import math

def psi_to_kgcm2(psi):
    #1 psi is equal to 0.0703070 kg per cm2
    return psi * 0.0703070
    
def kgcm2_to_psi(kgcm2):
    #1 psi is equal to 0.0703070 kg per cm2
    return kgcm2 / 0.0703070    

def circle_diameter_to_area(dia):
    r = dia / 2
    return math.pi * (r * r)
    
def area_of_circle(r):
    """Function that defines an area of a circle"""
    a = r**2 * math.pi
    return a
