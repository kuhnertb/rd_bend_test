#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 16:36:01 2016
@author: mcanulty
"""

#import calibData
import numpy as np
					#	D0	D1	D2	D3	D4	D5
arrayMap =  np.array(   [	
					[	78,	0,	1,	2,	3,	4	],		#S0
					[	5,	6,	7,	8,	9,	10	],		#S1
					[	11,	12,	13,	14,	15,	16	],		#S2
					[	78,	17,	18,	19,	20,	21	],		#S3
					[	22,	23,	24,	25,	26,	27	],		#S4
					[	28,	29,	30,	31,	32,	33	],		#S5
					[	78,	34,	35,	36,	37,	38	],		#S6
					[	39,	40,	41,	42,	43,	44	],		#S7
					[	78,	45,	46,	47,	48,	49	],		#S8
					[	50,	51,	52,	53,	54,	78	],		#S9
					[	55,	56,	57,	58,	59,	60	],		#S10
					[	61,	62,	63,	64,	65,	66	],		#S11
					[	67,	68,	69,	70,	71,	72	],		#S12
					[	73,	78,	74,	75,	76,	77	]	])	#S13


     
#kg = {
#
#    0: calibData.Sensor_1, 
#    1: calibData.Sensor_2, 
#    2: calibData.Sensor_3, 
#    3: calibData.Sensor_4, 
#    4: calibData.Sensor_5, 
#    5: calibData.Sensor_6, 
#    6: calibData.Sensor_7, 
#    7: calibData.Sensor_8, 
#    8: calibData.Sensor_9, 
#    9: calibData.Sensor_10, 
#    10: calibData.Sensor_11, 
#      
#}