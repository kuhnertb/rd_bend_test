#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 19:25:14 2016
@author: mcanulty
"""

import threading
import time, sys
import queue
import mido
import mido.backends.rtmidi  
import numpy as np
#from parseFromXLS_py3k import parseCalibFile_MinMax_nth_order

mido.set_backend('mido.backends.rtmidi')






class Bebop_Sensor_Device_Listener(threading.Thread):
    """ 
        Right now this is implemented for MIDI, but can be made general purpose
        by checking the device_description.device_type property
    """


    #*****************
    #***  Init  ******
    #*****************
    def __init__ (self, device_description, sniff=False, excel_calibration=False, dtype="uint16"):
        threading.Thread.__init__(self)
        global sensor_array
        
        self.dev_desc = device_description
        self.arr  = np.zeros(self.dev_desc.num_sensors, dtype)
        self.calib_arr  = np.zeros(self.dev_desc.num_sensors, dtype)
        self.sensor_dict =  self.dev_desc.dict_of_sensor_name_to_index
        self.given_name = self.dev_desc.device_name
        self.connected = False
        self.sniff = sniff
        self.dict_sensor_names = {}
        self.dtype = dtype
        
        
        
        if excel_calibration != False:
            self.excel_calibration = True
            self.spreadsheet_name = excel_calibration
            self.algs, self.minmax_dict = parseCalibFile_MinMax_nth_order(excel_calibration)
        else:
            self.excel_calibration = False
        
        #***  Check for encoding type  ***
        if self.dev_desc.data_encoding == 'bebop_midi_xy_int':
            self.decode_data = self.midi_int_x_y_encoding
            self.max_adc = 1024
        elif self.dev_desc.data_encoding == 'bebop_midi_xy_7bit':
            self.decode_data = self.midi_7bit_x_y_encoding
            self.max_adc = 127
        else:
            print("device encoding not valid:", self.dev_desc.data_encoding)
            
        if self.dev_desc.device_type == 'Fabric_Sensor_One':
            array_size = 16
            dtype = []
            for i in range(array_size):
                dtype.append(('    ' + str(i) + '    ', 'uint16'))
                
            self.xy_array = np.zeros(shape=(16, 16), dtype="uint16")
            #self.xy_array = np.zeros([array_size], dtype=dtype)
            
        
        self.senseNum = 0
        self.driveNum = 0          
        
    
    def change_encoding(self, encoding):
        if encoding == 'fabric_sensor_16_bit':
            self.program_change(1)
            self.decode_data = self.midi_int_x_y_fabric_sensor_encoding
            self.max_adc = 1024
            
        elif encoding == 'fabric_sensor_7_bit':
            self.program_change(0)
            self.decode_data = self.midi_7bit_x_y_encoding
            self.max_adc = 127
    
    #**********************************
    #***  return pointer to array  ****
    #**********************************
    def return_pointer_to_array(self):
        return self.arr
    def return_pointer_to_calib_array(self):
        return self.calib_arr
    def return_pointer_to_xy_array(self):
        return self.xy_array        

    #*****************
    #***  Close  *****
    #*****************        
    def close(self):
        self.running = False
    
    #*****************
    #***  Run ********
    #*****************     
    def run(self):
        self.running = True
        inputs = mido.get_input_names()

        print("\n\n\n" , "Checking midi ports...")
        print("Available inputs are:", inputs)
        
        
        #added in for compatibility with windows.  Finds first matching midi device name
        self.device_name = next((string for string in inputs if self.given_name in string), None)
        

        if self.device_name in inputs:
            midiLoop = True

            print("\n", self.device_name, " available, opening port...")
            sys.stdout.flush()              # Have to flush stdout here, or else
                                            # the output gets held up until later
            self.inport = mido.open_input(self.device_name)
            self.outport = mido.open_output(self.device_name)
            self.connected = True
        else:
            midiLoop = False
            self.inport = None

        #**************************
        #**** MAIN THREAD LOOP  ***
        #**************************
        self.x = 0
        self.y = 0
        
        self.adc = np.zeros(1, 'float')
        
        while midiLoop is True:

            time.sleep(0.01)
            #*******************
            self.decode_data()

            if self.running is False:
                midiLoop = False
        #**************************
        #**** CLEANUP  ************
        #**************************
        if self.inport != None:
            self.inport.close()
            self.outport.close()
            print(self.device_name + ': Midi port closed')
        else:
            print("No device to close")
            
    #*****************************************************************
    #*****************************************************************
    #*****************************************************************
    
    def midi_7bit_x_y_encoding(self):
        for msg in self.inport.iter_pending():  #without iter_pending, this blocks like a while loop
            try:
                col =  15 - msg.channel
                row =  msg.control
                self.adc[0] = msg.value
                sensor_name = str(row) + '_' + str(col)
    
                try:
                    sensor_array_num = self.sensor_dict[sensor_name]
                except:
                    
                    print("err:", sensor_name, self.adc[0])
                else:
                    self.arr[sensor_array_num] = self.adc[0]
                    self.xy_array[row][col] =  self.adc[0]
            except Exception as e:
                print(e)

    
    def midi_int_x_y_fabric_sensor_encoding(self):
        for msg in self.inport.iter_pending():  
   
            try:
                if(msg.channel == 15):
                    self.senseNum = msg.value 
                    self.driveNum = msg.control                
                else:
                    self.adc[0] = (     ((msg.channel & 0x7F) << 14)
                                |   ((msg.control & 0x7F) << 7)
                                |    (msg.value & 0x7F))
                    
                    row = self.senseNum
                    col = 15 - self.driveNum
                    
                    sensor_name = str(row) + '_' + str(col)
                    try:
                        sensor_array_num = self.sensor_dict[sensor_name]
                        
                    except Exception as e:
                        print(e)
                        print("err:", sensor_name, self.adc[0])
                    else:
                        self.arr[sensor_array_num] = self.adc[0]
                        self.xy_array[row][col] =  self.adc[0]
            except Exception as e:
                print(e)
    
    
    #*****************************************************************
    #*****************************************************************
    #*****************************************************************
    def midi_int_x_y_encoding(self):
        for msg in self.inport.iter_pending():  #without iter_pending, this blocks like a while loop

            if(msg.channel == 15):
                self.senseNum = msg.value 
                self.driveNum = msg.control                
            else:
                self.adc[0] = (     ((msg.channel & 0x7F) << 14)
                            |   ((msg.control & 0x7F) << 7)
                            |    (msg.value & 0x7F))

                
                
                sensor_name = 's' + str(self.senseNum) + '_' + 'd' + str(self.driveNum)
    
    
                #***  Sniff method records names of active sensors
                if(self.sniff):
                    self.dict_sensor_names[sensor_name] = ''
    
    
    
    
                try:
                    sensor_array_num = self.sensor_dict[sensor_name]
                    
                except Exception as e:
                    print(e)
                    print("err:", sensor_name, self.adc[0])
                else:
                    self.arr[sensor_array_num] = self.adc[0]
    
    
                if self.excel_calibration:    
                    try:
                        sensor_array_num = self.sensor_dict[sensor_name]
                        regLine = self.algs[sensor_name]
                        minmax = self.minmax_dict[sensor_name]
                        sensor_min = minmax[0]
                        sensor_max = minmax[1]                        
                    except Exception as e:
                        #print(e)
                        #print("err:", sensor_name, adc)
                        pass
                    else:
                        self.adc = self.adc.clip(sensor_min, sensor_max) 
                        pressure_data = np.polyval(regLine, self.adc)
                        self.calib_arr[sensor_array_num] = pressure_data
    
                    #print(sensor_name, adc)
                    
                    
    def control_message(self, val, cc, chan):
        msg = mido.Message('control_change', channel=chan, control=cc, value=val)
        self.outport.send(msg)
        
    def program_change(self, program,chan=0):
        msg = mido.Message('program_change', channel=chan, program=program)
        self.outport.send(msg)    
        
    def send_8bit_cc(self, val_8bit, chan=0):
        if val_8bit > 0xFF:
            val_8bit = 0xFF
        
        chan = chan
        cc =  (0x80 & val_8bit) >> 7
        val = 0x7F & val_8bit
            
        msg = mido.Message('control_change', channel=chan, control=cc, value=val)
        self.outport.send(msg)
        #print('sending', val_8bit)

#==============================================================================
#==============================================================================








midi_thread = None    





if __name__ == '__main__':
    
    
    from bebop.sensors.device_descriptions.wheelchair import device_descriptor 
    
    
    listener_thread = Bebop_Sensor_Device_Listener(device_descriptor, sniff=False)
    listener_thread.start()

    data_array =  listener_thread.return_pointer_to_array()
    count = 0
    try:
        while True:
            time.sleep(1)
            print(data_array)
            if(listener_thread.sniff):
                #print('here')
                print(listener_thread.dict_sensor_names.keys())
                
    except KeyboardInterrupt:
        pass
    
    midi_thread.close()

    time.sleep(1)


