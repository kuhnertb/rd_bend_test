#!/usr/bin/env python
#coding:utf-8

from bebop.sensors.bebop_sensor_device import Bebop_Sensor_Device_Descriptor
from midiSensor.sensor_define_generators import alicat_csv_titles_with_metric, gen_sensor_names


array_of_indexed_sensor_names = gen_sensor_names(0,15, 0,15)
num_sensors = len(array_of_indexed_sensor_names)


fabric_sensor_device_dict = {
    'midi_device_name': 'Fabric_Sensor_One', 
    'num_sensors': num_sensors,
    'array_of_indexed_sensor_names': array_of_indexed_sensor_names, 
    'leftmost_csv_header_string': alicat_csv_titles_with_metric + 'Digipot,',
    'device_type' : 'Fabric_Sensor_One', 
    'data_encoding' : 'bebop_midi_xy_7bit'
}


Fabric_Sensor_descriptor = Bebop_Sensor_Device_Descriptor(fabric_sensor_device_dict)

device_descriptor = Fabric_Sensor_descriptor

if __name__ == "__main__":

    print(device_descriptor.csv_header, end='')
    print(device_descriptor.first_sensor_column_in_csv)   
    print(device_descriptor.dict_of_sensor_name_to_index)
    print(device_descriptor.device_type, device_descriptor.data_encoding)
    
