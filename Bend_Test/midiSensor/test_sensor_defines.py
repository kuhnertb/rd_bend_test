#!/usr/bin/env python
#coding:utf-8
"""
Created on 01/19/18
@author:  mcanulty
"""


from midiSensor.sensor_define_generators import gen_sensor_names, create_csv_titles, create_sensor_dict,  alicat_csv_titles







    
    
    

def run_code_generators():
    
    global array_of_indexed_sensor_names
    global dict_of_sensor_name_to_index
    global csv_header
    global num_sensors
    
    
    array_of_indexed_sensor_names = gen_sensor_names(0, 14, 0, 14)       

    dict_of_sensor_name_to_index = create_sensor_dict(array_of_indexed_sensor_names)
    csv_header = alicat_csv_titles + create_csv_titles(array_of_indexed_sensor_names)    
    
    num_sensors = len(array_of_indexed_sensor_names)
    
    #print('num_sensors is', num_sensors)
    assert num_sensors == len(array_of_indexed_sensor_names)
    assert num_sensors == len(dict_of_sensor_name_to_index)
    
    

run_code_generators()















