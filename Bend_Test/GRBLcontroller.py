"""
Created on 09/11/2018
@author:  Douglas Bahr
"""


#!/usr/bin/env python
#coding:utf-8
"""
Created on 06/27/17
@author:  mcanulty
"""

import serial
import serial.tools.list_ports
import time, sys


portname = 'usbmodem26211'


#==============================================================================
#
#==============================================================================

def return_first_arduino_port():
	print('here')
	ports =  serial.tools.list_ports.comports()


	for port in ports:
		pName = port[0]
		print(pName)
		if portname in pName:
			print('port found:', pName)
			return pName

	print('no Arduino found')
	return None



#==============================================================================
#  GRBL DEVICE CLASS
#==============================================================================



class GrblClass(object):
	"""
	Connects to Grbl

	Takes optional serial portname in constructor

	might need to be tweaked slightly for windows
	"""

	def __init__(self, portname = portname):
			self.portname = portname

			self._errorCount = 0
			self.device_port = None
			self.ConnectStatus = self.connect()
			
			self.xmove(15)


		#******************************************
		#*******   CONNECT TO SERIAL DEVICE  ******
		#******************************************
	def connect(self):

		print('connecting to device')
		if not self.open(self.portname, 115200):
			self.device_port = None

		# GRBL doesn't respond for a couple of seconds so wait
		print('Awaiting Connection')
		time.sleep(1)
		#if not self.expects('', 'Grbl 1.1g',wait=2):
			##self._motor.pass_check = False
			#print('GRBL ID check failed')
			#return False
		#else:
			##self._motor.pass_check = True
			#print('GRBL ID check passed, device connected')
			#return True



	#******************************
	#*******  OPEN FUNCTION  ******
	#******************************
	def open(self, device, baudrate):
		'''   Opens a serial port to GRBL   '''
		if self.device_port:
			self.close()

		try:
			self.device_port = serial.Serial(port=device, baudrate=baudrate)
		except:
			raise NameError('Could not open serial port to GRBL')

		return True


	#*****************************************
	#*******  EXPECTS FUNCTION  ******
	#*****************************************
	def expects(self, out_message, expect_message, wait=3, delayInc=0.1):

		message_received = False
		#try:
		#***  SEND MESSAGE ***
		self.device_port.write(out_message.encode())

		#***  WAIT FOR SPECIFIED TIME FOR MESSAGE RESPONSE  ***
		while (wait > 0 and message_received == False):

			time.sleep(delayInc)
			wait -= delayInc


			#***  streams in a byte string, checks for message
			out = ''
			while self.device_port.inWaiting() > 0:
				x =  self.device_port.read(1).decode()
				out += x
				#print(x, end='')
			if expect_message in out:
				message_received = True



		#***  RETURN SUCCESS OR FAILURE  ***
		if message_received == False:
			#print('expected response not received after command:', out_message)
			print(out)
		return message_received


		#except Exception as e:
			#print(e)
			#raise NameError('Could not read/write with GRBL serial port')


	def Read_The_Serial(self, out_message = '?\n',wait=3, delayInc=0.1):
		
		self.device_port.write(out_message.encode())
		
		
		#***  WAIT FOR SPECIFIED TIME FOR MESSAGE RESPONSE  ***
		while (wait > 0 ):

			time.sleep(delayInc)
			wait -= delayInc

			#***  streams in a byte string, checks for message
			out = ''
			Lovely = False
			while self.device_port.inWaiting() > 0:
				x =  self.device_port.read(1).decode()
				

				if (x == '<' or Lovely == True):
					out+=x
					Lovely = True
				if(x == '>'):
					break
				if(x == 'A'):
					alarm = x + str(self.device_port.read(6).decode())
					print('******** ' + str(alarm) + ' Triggered **********')
					mes = 'ALARM'
					return mes
				now = time.time()
			#print(out)
			
			
			
			if ('<' in out and '>' in out):
				out = out[1:-3]
				out = str(out).split(',')
				return(out)
			
		
		print('expected response not received after command:', out_message)
	
	def get_pos(self):
		
		query = self.Read_The_Serial()
		
		if (query == 'ALARM'):
			pos = -99999
			state = True
		
		else:
			pos_l = query[0].split(":")
			pos = float(pos_l[1])
			state = pos_l[0][0:4]

		if state == "Idle":
			state = True
		else:
			state = False
			
		return pos, state


	#************************************
	#*******   CLOSE SERIAL DEVICE  *****
	#************************************

	def close(self):

		self.device_port.close()
		self.device_port = None
		print("closed grbl device on port", self.portname)


	#**********************************
	#*******   MOVEMENT COMMANDS  *****
	#**********************************

	def home(self):
		print('Home command initiated')
		self.expects('$H\n', 'ok')
		
		#query = self.Read_The_Serial()
		#pos = query[0].split(':')
		#return float(pos[1]) 


	def setFeedSpeed(self, speed):
		speed = int(speed)
		#if speed > 30000:
			#speed = 30000

		if speed > 2500:
			speed = 2500

		if speed < 0:
			speed = 0

		command = 'F' + str(speed) + '\n'
		return self.expects(command, 'ok'), time.time()

	def xmove(self, loc):
		#print moving to, loc
		#self.setFeedSpeed(speed)

		loc = int(loc)
		#if loc > 100:
			#loc = 100


		command = 'X' + str(loc) + '\n'
		#print(command)

		return self.expects(command, 'ok'), time.time()

	def GrblCommand(self,text,wait = 1):

		print(self.expects(text+'\n','ok',wait))

class XMove(GrblClass):

	def __init__(self):
		self.stepper = GrblClass(portname=return_first_arduino_port())
		self.Xpos = self.stepper.home()
		time.sleep(.5)
		#self.stepper.xmove(self.Xpos)
		time.sleep(.5)

	def Xinput(self):
		while(True):
			Temp = self.Xpos
			try:
				self.Xpos = input("XPos?")
			except:
				print("Not a number, idiot!")
				self.stepper.close()
				break

			if(Temp == self.Xpos):
				self.stepper.close()
				break

			else:
				print(self.Xpos)
				self.stepper.xmove(self.Xpos)


if __name__ == "__main__":
	
	m = GrblClass(portname = return_first_arduino_port())
	#time.sleep(2)
	m.home()
	time.sleep(.5)
	
	xspeed = 10000            #limit at 15000 
	speed_message = '$110=' + str(xspeed) + '\n'
	
	xaccel = 5000             # limit at 5000
	accel_message = '$120=' + str(xaccel) + '\n'	
	
	m.device_port.write(speed_message.encode())
	time.sleep(.1)
	m.device_port.write(accel_message.encode())
	time.sleep(.1)
	
	origin, state = m.get_pos()
	

	#time.sleep(.3)
	for j in range(5):
		m.xmove(origin + 180)
		state = False
		while(not state):
			then = time.time()
			pos, state = m.get_pos()
			print(str(time.time() - then))
				
		#time.sleep(.5)
		m.xmove(origin)
		
		state = False
		while(not state):
			pos, state = m.get_pos()
				
		#time.sleep(.5)
	
	
