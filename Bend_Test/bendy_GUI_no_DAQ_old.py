"""
Automated Bend Test + GUI

Created on 03/20/2019
@author:   Victor Lalo

"""


from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QDateTime, QTimer, QThread, QSize, QRect
#from PySide2 import QtCore, QtWidgets, QtGui

from time import time, sleep
import sys, serial
import pandas as pd
import numpy as np
from threading import Thread

#from dual_board_DAQ import dual_board_listener
from GRBLcontroller import GrblClass

#df_header = ['time', 'crease_number', 'angle', 'S5_adc', 'S5_res','S10_adc', 'S10_res','S7_adc', 'S7_res','S12_adc', 'S12_res',]


def record(m, adc, startTime, outArray, zero, crease, r_bias = 2000):
    posi, state = m.get_pos()
    
    if (posi == -99999):
        return posi, state, outArray        
    
    angle = round((posi - zero) * 1.8, 2)
    
    adc_res = []
    
    for a in adc:
        if a == 0:
            a = 1
            
        r = ((1023 * r_bias)/a) - r_bias      # Rf=((1023*Rb)/ADCf)-Rb
        adc_res.append([a,r])
    
    now = round((time() - startTime), 2)
    
    outline=np.hstack((now, crease, angle))
    outline.append(adc_res)
    outArray=np.vstack((outArray,outline.T))
    
    return posi, state, outArray

def convert_to_hr_min_sec(sec):
    hours = str(int(sec/3600))
    minutes = str(int((sec%3600)/60))
    seconds = str(int((sec%3600)%60))
    
    if len(minutes) == 1:
        minutes = '0' + minutes
    if len(seconds) == 1:
        seconds = '0' + seconds
    
    hr_min_sec = hours + ':' + minutes + ':' + seconds
    
    return hr_min_sec

        
class bendyUI(QWidget):
    def __init__(self, parent = None):
        #super(creaseUI, self).__init__(return_first_arduino_port)
        super(bendyUI, self).__init__(parent)
        
        ### motor ####
        self.motor = None
        self.port = None
        
        ###### VARIABLES ######
        self.m_speed = 5000
        self.m_accel = self.m_speed * 0.9
        
        self.position = 0
        
        self.num_bends = 5
        self.est_time = convert_to_hr_min_sec(0)
        
        self.max_angle = 90
        self.origin = 0
        self.full_bend = 90
        
        self.hold_time = 1
        
        #self.r_bias = 2000
        #self.outArray = np.zeros((1,5))
        
        self.filename = 'test0'
        self.connected = False
        
        self.setupUi()
        self.listPorts()
        self.connect()
        self.setHoldTime_w_Text()
        
    def setupUi(self):
        
        ### SET UP GUI LAYOUT ####
        self.createCommandBox()
        self.createParameterBox()
        self.createComBox()
        #self.createAdvancedBox()
        
        self.bendsLabel = QLabel('Current Bend: 0 of ' + str(self.num_bends))
        self.timeLeftLabel = QLabel('Estimated Time Left: ' + str(self.est_time))
        
        # Progress bar and est. time left text box
        self.progressBar = QProgressBar()
        self.progressBar.setMinimum(0)        
        
        mainLayout = QGridLayout()
        mainLayout.addWidget(self.CommandBox, 0, 0)
        mainLayout.addWidget(self.ParameterBox, 0, 1)
        mainLayout.addWidget(self.ComBox, 1, 0, 1, 2)
        
        mainLayout.addWidget(self.progressBar, 2, 0)
        mainLayout.addWidget(self.bendsLabel, 3, 0)
        mainLayout.addWidget(self.timeLeftLabel, 3, 1)
        
        self.setLayout(mainLayout)
        QApplication.setStyle(QStyleFactory.create('Mac'))
        
        
        #### ACTIVATE BUTTONS #####
        #self.homeButton.clicked.connect(self.homing)
        self.connectButton.clicked.connect(self.connect)
        self.startButton.clicked.connect(self.startTest)
        self.stopButton.clicked.connect(self.stopTest)
        
        #self.saveButton.clicked.connect(self.saveCSV)
        
        self.scanPortsButton.clicked.connect(self.listPorts)
        self.portsComboBox.activated[str].connect(self.chosePort)
        
        
        #### ACTIVATE TEXTBOXES ####
        
        self.mSpeedText.valueChanged.connect(self.setSpeed_w_Text)
        self.nBendsText.valueChanged.connect(self.setBendsNum)
        self.maxAngleText.valueChanged.connect(self.setAngle_w_Text)
        self.holdTimeText.valueChanged.connect(self.setHoldTime_w_Text)
         
          
        #### ACTIVATE SLIDERS/DIALS #####
        self.mSpeedSlider.sliderMoved.connect(self.setSpeed_w_Slider)
        self.holdTimeSlider.sliderMoved.connect(self.setHoldTime_w_Slider)
        self.maxAngleDial.sliderMoved.connect(self.setAngle_w_Dial)
         
        
        
    ############# command/setup buttons #############
    def createCommandBox(self):
        self.CommandBox = QGroupBox('Commands')
        
        #self.homeButton = QPushButton('Home')
        self.connectButton = QPushButton('Connect')
        self.startButton = QPushButton('Start Test')  ## make toggle button btwn start and pause
        self.stopButton = QPushButton('Stop Test')
        
        self.scanPortsButton = QPushButton('Scan Ports')
        self.portsComboBox = QComboBox()
        
        # LAYOUT #
        layout = QGridLayout()
        
        layout.addWidget(self.portsComboBox, 0, 0)
        layout.addWidget(self.scanPortsButton, 0, 1)
        layout.addWidget(self.connectButton, 0, 2)
        
        layout.addWidget(self.startButton, 1, 1)
        layout.addWidget(self.stopButton, 1, 2)
        
        self.CommandBox.setLayout(layout)
        
        
    ######### parameter sliders #############
    def createParameterBox(self):
        self.ParameterBox = QGroupBox('Parameters')
        
        ### Motor Speed
        mSpeedLabel = QLabel('Motor Speed (mm/s)')
        self.mSpeedSlider = QSlider(Qt.Horizontal, self.ParameterBox)
        self.mSpeedSlider.setRange(500, 50000)
        self.mSpeedSlider.setValue(self.m_speed)
        
        self.mSpeedText = QSpinBox(self.ParameterBox)
        self.mSpeedText.setMaximum(50000)
        self.mSpeedText.setValue(self.m_speed)
        
        #mSpeedButton = QPushButton('Set Speed')
        
        
        ### Number of Creases
        nBendsLabel = QLabel('Number of Bends')
        self.nBendsText = QSpinBox(self.ParameterBox)
        self.nBendsText.setMaximum(100000)
        self.nBendsText.setValue(self.num_bends)
        
        ### Max Angle
        maxAngleLabel = QLabel('Bend Angle (Degrees)')
        self.maxAngleDial = QDial(self.ParameterBox)
        self.maxAngleDial.setValue(self.max_angle)
        self.maxAngleDial.setNotchesVisible(True)
        self.maxAngleDial.setRange(0, 360)
        
        self.maxAngleText = QSpinBox(self.ParameterBox)
        self.maxAngleText.setRange(1,360)
        self.maxAngleText.setValue(self.max_angle)
        
        
        ### Hold Time
        holdTimeLabel = QLabel('Hold Time (ms)')
        self.holdTimeSlider = QSlider(Qt.Horizontal, self.ParameterBox)
        self.holdTimeSlider.setRange(1, 3000)
        self.holdTimeSlider.setValue(self.hold_time)
        
        self.holdTimeText = QSpinBox(self.ParameterBox)
        self.holdTimeText.setRange(1,3000)
        self.holdTimeText.setValue(self.hold_time)
        
        
        # LAYOUT #
        layout = QGridLayout()
        layout.addWidget(nBendsLabel, 0,0)
        layout.addWidget(self.nBendsText, 0,1)
        
        layout.addWidget(maxAngleLabel, 1,0)
        layout.addWidget(self.maxAngleDial, 1,1)
        layout.addWidget(self.maxAngleText, 1,2)        
        
        layout.addWidget(mSpeedLabel, 2,0)
        layout.addWidget(self.mSpeedSlider, 2,1)
        layout.addWidget(self.mSpeedText, 2,2)
        
        layout.addWidget(holdTimeLabel, 3,0) 
        layout.addWidget(self.holdTimeSlider, 3,1)
        layout.addWidget(self.holdTimeText, 3,2)
            
        self.ParameterBox.setLayout(layout)
        
        ######### text ################
    def createComBox(self):
        self.ComBox = QGroupBox('Communication')
        
        
        # Comments Text Box
        self.commentBox = QLabel()
        self.commentBox.setGeometry(QRect(0, 0, 100, 100)) #(x, y, width, height)
        self.commentBox.setStyleSheet('color: black')
        self.commentBox.setStyleSheet('background-color: white')        
        self.commentBox.setText('Welcome! Begin by connecting to a proper Arduino port (begins with "usbmodemXXXX") \n')
        
        
        # Filename input and Save Button
        #self.fileNameInput = QLineEdit('Filename')
        #self.saveButton = QPushButton('Save')
        
        
        # LAYOUT #
        layout = QGridLayout()
        layout.addWidget(self.commentBox, 0,0, 0, 1)
        
        #layout.addWidget(self.fileNameInput, 0,1)
        #layout.addWidget(self.saveButton, 1,1)
        
        self.ComBox.setLayout(layout)
        
        
    def listPorts(self):
        port_list = []
        p_list = serial.tools.list_ports.comports()
        for p in p_list:
            port_list.append(p[0])
        port_list.reverse()
        
        self.portsComboBox.clear()    
        self.portsComboBox.addItems(port_list)
        self.port = port_list[0]
        
    def chosePort(self, portName):
        self.port = portName
        
    def connect(self):
        if self.port:
            if 'usbmodem' in self.port:
                self.motor = GrblClass(portname = self.port) 
                self.connected = True
                self.commentBox.setText('Connected. Verify parameter values and click the "Start" button to begin test.')
            else:
                self.commentBox.setText('Selected port is not an Arduino. Please select the correct port. \nArduino ports register as "usbmodemXXXXX".')
    
    def homing(self):
        if self.connected:
            self.commentBox.setText('Homing...')
            
            self.motor.home()
            sleep(2)
            
            self.origin, state = self.motor.get_pos()           
            self.origin += 2.00
            
            self.homed = True
            
            self.commentBox.setText('Home found. Origin is at ' + str(self.origin) +'\nReady to start test. Verify all parameters and click the "Start Test" button.') 
        else:
            self.commentBox.setText('Must connect to the motor before homing. \nSelect a port from the drop down list.')
        
    def startTest(self):
        if self.connected:
            self.commentBox.setText('Setting parameters...')  
            #sleep(2);
            
            self.progressBar.setMaximum(self.num_bends)
            self.progressBar.setValue(0)
            
            speed_message = '$110=' + str(self.m_speed) + '\n'
            accel_message = '$120=' + str(self.m_accel) + '\n'
            
            self.motor.device_port.write(speed_message.encode())
            sleep(.25)
            self.motor.device_port.write(accel_message.encode()) 
            sleep(.25)
            
            self.origin, state = self.motor.get_pos()
            angle = float(self.max_angle)
            self.full_bend = self.origin + angle   

            #sleep(1)
            
            self.startTime = time()
            self.commentBox.setText('Beginning test run... \n')
            
            Thread(target=self.Start_Thread).start()
            
        else:
            self.commentBox.setText('Not Connected to a motor system.\nInitialize system by clicking the "Connect" button.')
            
    def Start_Thread(self):    
        time_avg_array = [0,0,0,0,0,0,0,0]
        
        for i in range(self.num_bends):
            time_then = time()
            
            self.run = i + 1
            self.bendsLabel.setText('Current Bend: ' + str(self.run) + ' of ' + str(self.num_bends))
            sleep(0.01)
            self.progressBar.setValue(self.run)
            sleep(0.01)
            print(self.run)
            
            # ----- Move to full bend point ---- #
            
            state = False
            self.motor.xmove(self.full_bend)
            
            while (not state):
               #self.position, state, self.outArray = record(self.motor, midi.midi, self.startTime, self.outArray, self.zero, self.run)
                
                self.position, state = self.motor.get_pos()
                
                if (self.position == -99999):
                    break
                else:
                    sleep(0.025)
            if (self.position == -99999):
                break
        
            sleep(self.hold_time)
            
            
            # ----- Move to origin point ---- #
            state = False
            self.motor.xmove(self.origin)
            
            while (not state):
                #self.position, state, self.outArray = record(self.motor, midi.midi, self.startTime, self.outArray, self.zero, self.run)
                
                self.position, state = self.motor.get_pos()
                
                if (self.position == -99999):
                    break
                else:
                    sleep(0.025)
            if (self.position == -99999):
                break
            sleep(self.hold_time)
            
            time_now = time() - time_then
            
            if i < 8:
                time_avg = time_now
                time_avg_array[i] = time_now
            else:
                time_avg_array[i%8] = time_now
                time_avg = sum(time_avg_array) / 8.0000
                
            self.est_time = convert_to_hr_min_sec(time_avg * (self.num_bends - self.run))
            
            self.timeLeftLabel.setText('Estimated Time Left: ' + self.est_time)            
            
        self.motor.xmove(self.origin)
        
        now = round((time() - self.startTime), 2)
        hr_min_sec = convert_to_hr_min_sec(now)
        self.commentBox.setText('Test complete.\nSample bent ' + str(self.num_bends) + ' times.\nTest duration (hr:min:sec):  ' + str(hr_min_sec)) 
        
        
    def stopTest(self):
        self.motor.close()
        #midi.close()
        sys.exit(app.exec_())
        
    def saveCSV(self):
        df = pd.DataFrame(self.outArray, columns = df_header)
        
        counter = 0
        for row in df:
            if row['adc'] == 0:
                counter += 1
        df = df[counter:]
        
        #additional dataframe processing goes here (if wanted)
        #analyzeAndPlot(df)
        
        saveName = self.fileNameInput.text() + '.csv'
        
        df.to_csv(saveName, index = False)
        
        self.commentBox.clear()
        self.commentBox.setText('Test saved to the working directory.')
        
        
    def setSpeed_w_Slider(self):
        self.m_speed = self.mSpeedSlider.value()
        self.mSpeedText.setValue(self.m_speed)
    def setSpeed_w_Text(self):
        self.m_speed = self.mSpeedText.value()
        self.mSpeedSlider.setValue(self.m_speed)    
        
    def setBendsNum(self):
        self.num_bends = self.nBendsText.value()
        self.progressBar.setMaximum(self.num_bends)
        self.bendsLabel.setText('Current Bend: 0 of ' + str(self.num_bends))
        
    def setAngle_w_Dial(self):
        self.max_angle = self.maxAngleDial.value()
        self.maxAngleText.setValue(self.max_angle)      
    def setAngle_w_Text(self):
        self.max_angle = self.maxAngleText.value()
        self.maxAngleDial.setValue(self.max_angle)          
        
        
    def setHoldTime_w_Slider(self):
        self.hold_time = self.holdTimeSlider.value()
        self.holdTimeText.setValue(self.hold_time)
        self.hold_time = float(self.hold_time)/1000.00
    def setHoldTime_w_Text(self):
        self.hold_time = self.holdTimeText.value()
        self.holdTimeSlider.setValue(self.hold_time)
        self.hold_time = float(self.hold_time)/1000.00
        
    

if __name__ == "__main__":
    
    app = QApplication(sys.argv)  
    
    UI = bendyUI()
    UI.show()

    sys.exit(app.exec_())
