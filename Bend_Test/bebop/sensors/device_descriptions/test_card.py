#!/usr/bin/env python
#coding:utf-8

from bebop.sensors.bebop_sensor_device import Bebop_Sensor_Device_Descriptor
from midiSensor.sensor_define_generators import alicat_csv_titles_with_metric, gen_sensor_names


array_of_indexed_sensor_names = ['s0_d1', 's1_d1', 's2_d1', 's3_d1'] #['s0_d0', 's0_d1', 's0_d2', 's0_d3','s1_d0', 's1_d1', 's1_d2', 's1_d3','s2_d0', 's2_d1', 's2_d2', 's2_d3'] #gen_sensor_names(0,2, 0,3)
num_sensors = len(array_of_indexed_sensor_names)


test_card_device_dict = {
    'midi_device_name': 'Test_Card', 
    'num_sensors': num_sensors,
    'array_of_indexed_sensor_names': array_of_indexed_sensor_names, 
    'leftmost_csv_header_string': alicat_csv_titles_with_metric,
    #'device_type' : 'Fabric_Sensor_One', 
    'data_encoding' : 'bebop_midi_xy_int'
}


test_card_descriptor = Bebop_Sensor_Device_Descriptor(test_card_device_dict)

device_descriptor = test_card_descriptor

if __name__ == "__main__":

    print(device_descriptor.csv_header, end='')
    print(device_descriptor.first_sensor_column_in_csv)   
    print(device_descriptor.dict_of_sensor_name_to_index)
    print(device_descriptor.device_type, device_descriptor.data_encoding)
    
