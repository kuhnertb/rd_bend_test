#!/usr/bin/env python
#coding:utf-8
"""
Created on 01/19/18
@author:  mcanulty
"""

alicat_csv_titles = 'seconds,SetPoints,Alicat,Manometer,'
alicat_csv_titles_with_metric = 'seconds,SetPoints,Alicat,Manometer_g/cm2,Manometer_PSI,'

def gen_sensor_names(x_sense_start, x_sense_end, y_drive_start, y_drive_end, prefix=''):
    '''   create list of arrays of sensors with [adc_sense_drive]   '''
    sensor_array = []
    for x in range(x_sense_start, x_sense_end+1):
        for y in range(y_drive_start, y_drive_end+1):
            sensor_name = prefix + str(x) + '_' + str(y)
            sensor_array.append(sensor_name)    
    #print(sensor_array)
    return sensor_array

def gen_sensor_names_with_sense_drive(x_sense_start, x_sense_end, y_drive_start, y_drive_end, prefix=''):
    '''   create list of arrays of sensors with [prefix_sX_dY]   '''
    sensor_array = []
    for x in range(x_sense_start, x_sense_end+1):
        for y in range(y_drive_start, y_drive_end+1):
            sensor_name = prefix + "s" + str(x) + '_' + "d" + str(y)
            sensor_array.append(sensor_name)    
    #print(sensor_array)
    return sensor_array


def create_sensor_dict(array_of_indexed_sensor_names):
    dict_of_sensor_name_to_index = {}
    for i, name in enumerate(array_of_indexed_sensor_names):
        dict_of_sensor_name_to_index[name] = i
    #print(dict_of_sensor_name_to_index)
    return dict_of_sensor_name_to_index

def create_csv_titles(array_of_indexed_sensor_names):
    '''generates csv header, with no extra spaces, no unneeded quotes'''
    
    csv_string = ','.join(array_of_indexed_sensor_names) + '\n'
    
    #print(csv_string)
    return csv_string


















