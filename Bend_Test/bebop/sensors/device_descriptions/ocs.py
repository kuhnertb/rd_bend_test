#!/usr/bin/env python
#coding:utf-8

from bebop.sensors.bebop_sensor_device import Bebop_Sensor_Device_Descriptor
from midiSensor.sensor_define_generators import alicat_csv_titles_with_metric, gen_sensor_names_with_sense_drive

#array_of_indexed_sensor_names = ["0_0", "0_1", "0_2", "0_3", "0_4", "0_5", "1_0", "1_1", "1_2", "1_3", "1_4", "1_5", "2_0", "2_1", "2_2", "2_3", "2_4", "2_5", "3_0", "3_1", "3_2", "3_3", "3_4", "3_5", "4_0", "4_1", "4_2", "4_3", "4_4", "4_5", "5_0", "5_1", "5_2", "5_3", "5_4", "5_5", "6_0", "6_1", "6_2", "6_3", "6_4", "6_5", "7_0", "7_1", "7_2", "7_3", "7_4", "7_5", "8_0", "8_1", "8_2", "8_3", "8_4", "8_5", "9_0", "9_1", "9_2", "9_3", "9_4", "9_5", "10_0", "10_1", "10_2", "10_3", "10_4", "10_5", "11_0", "11_1", "11_2", "11_3", "11_4", "11_5", "12_0", "12_1", "12_2", "12_3", "12_4", "12_5", "13_0", "13_1", "13_2", "13_3", "13_4", "13_5"]

array_of_indexed_sensor_names = gen_sensor_names_with_sense_drive(0, 23, 0, 7)
num_sensors = len(array_of_indexed_sensor_names)


ocs_device_dict = {
    'midi_device_name': 'OCS_HD_Front', 
    'num_sensors': num_sensors,
    'array_of_indexed_sensor_names': array_of_indexed_sensor_names, 
    'leftmost_csv_header_string': alicat_csv_titles_with_metric, 
    'device_type' : 'bebop_midi_device', 
    'data_encoding' : 'bebop_midi_xy_int'    
}


device_descriptor = Bebop_Sensor_Device_Descriptor(ocs_device_dict)


if __name__ == "__main__":

    print(device_descriptor.csv_header, end='')
    print(device_descriptor.first_sensor_column_in_csv)
    print(device_descriptor.dict_of_sensor_name_to_index)
    print(array_of_indexed_sensor_names)
