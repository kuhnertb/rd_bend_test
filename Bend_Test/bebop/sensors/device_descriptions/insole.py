#!/usr/bin/env python
#coding:utf-8

from bebop.sensors.bebop_sensor_device import Bebop_Sensor_Device_Descriptor
from midiSensor.sensor_define_generators import alicat_csv_titles_with_metric, gen_sensor_names_with_sense_drive

#array_of_indexed_sensor_names = ['s0_d0', 's0_d1', 's0_d2', 's0_d3',\
#'s0_d4', 's0_d5', 's0_d6', 's0_d7', 's0_d11', 's0_d12', 's0_d13', 's1_d0',\
#'s1_d1', 's1_d2', 's1_d3', 's1_d4', 's1_d5', 's1_d6', 's1_d7', 's1_d8',\
#'s1_d9', 's1_d10', 's1_d11', 's1_d12', 's1_d13']

array_of_indexed_sensor_names = gen_sensor_names_with_sense_drive(0, 1, 0, 13)
num_sensors = len(array_of_indexed_sensor_names)


insole_device_dict = {
    'midi_device_name': 'Arm_Controller', 
    'num_sensors': num_sensors,
    'array_of_indexed_sensor_names': array_of_indexed_sensor_names, 
    'leftmost_csv_header_string': alicat_csv_titles_with_metric, 
    'device_type' : 'bebop_midi_device', 
    'data_encoding' : 'bebop_midi_xy_int'    
}


Insole_descriptor = Bebop_Sensor_Device_Descriptor(insole_device_dict)

device_descriptor = Insole_descriptor

if __name__ == "__main__":

    print(Insole_descriptor.csv_header, end='')
    print(Insole_descriptor.first_sensor_column_in_csv)
    #print(alicat_csv_titles_with_metric.split(sep=','))
    #print(first_sensor_position)
    
    print(Insole_descriptor.dict_of_sensor_name_to_index)
    
    print(array_of_indexed_sensor_names)
    
    
