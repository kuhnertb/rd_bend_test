import serial.tools.list_ports
import math
import csv
import time
import sys, time, threading
import numpy as np
#from PyQt5 import QtCore, QtGui, QtWidgets
from GRBLcontroller_crease import GrblClass, XMove, return_first_arduino_port
#from creasy_simple_test_cards import Ui_MainWindow

from bebop.sensors.bebop_sensor_device_listener import Bebop_Sensor_Device_Listener
from bebop.sensors.device_descriptions.force_glove import force_glove_descriptor as device_descriptor
#from bebop.sensors.device_descriptions.test_card import test_card_descriptor as device_descriptor

#csv_header = 'crease_number, s0_d1_rest, s0_d1_min, s0_d1_max, s1_d1_rest, s1_d1_min, s1_d1_max, s2_d1_rest, s2_d1_min, s2_d1_max, s3_d1_rest, s3_d1_min, s3_d1_max'
csv_header = 'time, crease_number, angle, adc \n'

class MidiDAQ(threading.Thread):
    #*****************
    #***  Init  ******
    #*****************
    
    def __init__ (self, filename = 'test'):
        threading.Thread.__init__(self)
        
        self.crease_num = 0
        self.angle = 0
        
        self.adc = 0
        
        self.filename = filename + '.csv'
        
        self.record = False

    #*****************
    #***  Close  *****
    #*****************
    def close(self):
        self.running = False

    #*****************
    #***  Run ********
    #*****************
    def run(self):
        self.running = True

        listener_thread = Bebop_Sensor_Device_Listener(device_descriptor)
        listener_thread.start()

        global midi_array
        midi_array = listener_thread.return_pointer_to_array()

        file = open('backup.csv', 'w')
        file.truncate()
        file.write(csv_header)
        
        self.startTime = time.time()
        saveTime = time.time()         
               
        #**************************
        #**** MAIN LOOP  **********
        #**************************
        while self.running:
            time.sleep(0.01)
            
            if time.time() - saveTime > 0.1:     # record ~ 10 samples per crease
                saveTime = time.time()
                
                if midi_array is not None:
                    now = time.time() - self.startTime
                    
                    if midi_array[1] > midi_array[15]:
                        self.adc = midi_array[1]
                    elif midi_array[15] > midi_array[1]:
                        self.adc = midi_array[15]   
                    
                    
                    pList = [now, self.crease_num, self.angle, self.adc]
                    #mList = midi_array.tolist()
                    
                    outLine = ','.join(map(str, pList)) + '\n'

                    file = open('backup.csv', 'a')
                    file.write(outLine)
                    file.close()
                        
                else:
                    print('no midi data')
                          
            
if __name__ == "__main__":

    #app = QtWidgets.QApplication(sys.argv)
    #UI = Ui_MainWindow()
    record_thread = MIDI_DATA('test_0.csv')

    record_thread.start()
    
    while(True):
        print(record_thread.adc)
    
    

    #sys.exit(app.exec_())
    print('record_here')
    running = True
    record_thread.close()
