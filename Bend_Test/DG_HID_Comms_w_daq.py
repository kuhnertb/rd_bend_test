#### hidapi and cython libraries required #####

import time, hid
import threading
import pandas as pd

def pad_zeros(li, total_length):
	for i in range(total_length - len(li)):
		li.append(0)
	return li
	
usb_comms_frame = pad_zeros([0, 0xF0, 0x00, 0x01, 0x01, 0xF7], 30)
stream_data_on_frame = pad_zeros([0, 0xF0, 0x01, 0x01, 0x01, 0xF7], 30) 

class gloveHID:
	
	def __init__(self):
		self.vid = None
		self.pid = None
		self.path = None
		self.name = None
		self.glove = None  
		
	def connect(self):
		for device in hid.enumerate():
			if 'Forte' in device['product_string']:
				self.path = device['path']
				self.vid = device['vendor_id']
				self.pid = device['product_id']
				self.name = device['product_string']
				
			#else:
				#print("No Forte Glove Found.")
				#return False
		if self.name == None:
			print("No Forte Glove Found.")
			return False
		
		#print(device['product_string'])
		self.glove = hid.device()
		self.glove.open(self.vid, self.pid)
		
		print("Connected to " + str(self.name))
		time.sleep(0.5)
		
		self.glove.write(usb_comms_frame)
		self.glove.write(stream_data_on_frame)        
		
	def readAll(self, timeout = 10):
		fullStream = self.glove.read(29, timeout_ms = timeout)
		data = fullStream[3:25]
		# Thumb1 Thumb2  Index1 Index2  Middle1 Middle2  Ring1 Ring2  Pinky1 Pinky2   IMU W (3 bytes)  IMU X (3 bytes)   IMU Y (3 bytes)   IMU Z (3 bytes)
		return data
	
	def readFingers(self, finger = None, timeout = 10):
		data = self.readAll()
		if finger == None:
			return data[:10]
		# THUMB
		elif finger == 0:
			return data[:2]
		# INDEX
		elif finger == 1:
			return data[2:4]
		# MIDDLE
		elif finger == 2:
			return data[4:6]
		# RING
		elif finger == 3:
			return data[6:8]
		# PINKY
		elif finger == 4:
			return data[8:10] 
		
	
	def readIMU(self, axis = None):
		data = self.readAll()
		if axis == None:
			return data[10:22]
		# IMU W
		elif axis == 0:
			return data[10:13]
		# IMU X
		elif axis == 1:
			return data[13:16]
		# IMU Y
		elif axis == 2:
			return data[16:19]
		# IMU Z
		elif axis == 3:
			return data[19:22]
		
	def readBattery(self):
		return self.glove.read(29)[25]
	
	
	def sendHapticCommand(self, finger=0x06, amp=0x60, loc=0x00, grain_size=0x7F, grain_fade=0x30, pitch=0x00, waveform=0x00, one_shot=0x01, trigger=0x01):
		hapticCommand = [0, 0xF0, 0x30, 0x0A, finger, amp, loc, loc, grain_size, grain_fade, pitch, waveform, one_shot, trigger, 0xF7]
		hapticCommand = pad_zeros(hapticCommand, 30)
		
		self.glove.write(hapticCommand)
		return
		
	def sendLEDCommand(self, command):
		ledCommand = [0, 0xF0, 0x05, 0x01, command, 0xF7]
		ledCommand = pad_zeros(ledCommand, 30)
		
		self.glove.write(ledCommand)
		return
	
	def sendCalibCommand(self, command):
		calibCommand = [0, 0xF0, 0x20, 0x01, command, 0xF7]
		calibCommand = pad_zeros(calibCommand, 30)
		
		self.glove.write(calibCommand)
		return
	
	
	#### LOOK INTO IT MORE. NOT DOING WHAT I WANT#####
	def requestGloveInfo(self):
		infoCommand = [0, 0xF0, 0x51, 0x00, 0xF7]
		infoCommand = pad_zeros(infoCommand, 30)
		
		self.glove.write(infoCommand)
		
		data = self.glove.read(29)
		print(data)
		return data
	
	

class DAQThread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.num_end_cycle= 0
		self.running=False
		self.cycle = 0
		self.outArray = None
		self.output_filename = None
	def close(self):
		self.running = False
		print('We out!')
		self.outArray.to_csv(self.output_filename,index=False)		
	def run(self):
		self.running = True
		bends = self.cycle    #This is where we put bends in
		df_header=['time (s)','bends','s1','s2','s3','s4','s5','s6','s7','s8','s9','s10']
		dateNumbers = time.strftime("%Y_%m_%d")
		self.output_filename = dateNumbers + "_DG_lyfe_cycle.csv"   

		glove = gloveHID()
		print('GloveConnected')
		glove.connect()
		self.outArray=pd.DataFrame([],columns=df_header)
		
		glove.sendLEDCommand(0x7F)
		time.sleep(0.5)
		glove.sendLEDCommand(0x00)
		low=10000     #beginning of interval probably want this to be bends        GUI
		top=low+5  #end of interval
		
		

		#glove.sendHapticCommand(waveform = 6)
		glove.requestGloveInfo()
		start_time=time.time()
		last=0
		while self.running == True:
			time.sleep(0.05)
			t=time.time()-start_time
			interval=self.cycle-last
			#print(self.cycle,interval,last)		
			if (interval)>= low and (interval)<=top:
				f = glove.readFingers()
				full=[t]+[self.cycle]+f
				outLine=pd.DataFrame([full],columns=df_header)
				self.outArray=self.outArray.append(outLine,ignore_index=False)
				print(full)
			elif interval>top:
				last=self.cycle
				print('Saved')
				self.outArray.to_csv(self.output_filename,index=False)
				

			if self.cycle >self.num_end_cycle:
				self.running == False
				break
		if self.outArray.size<50:
			print('Error no data')
		else:
			print('We out!')
			self.outArray.to_csv(self.output_filename,index=False)

if __name__ == '__main__':
	DAQ=DAQThread()
	try:
		DAQ.run()
	except KeyboardInterrupt:
		pass
	
	while True:
		pass
	#print([g[1], g[4], g[7], g[10]])
	#time.sleep(0.1)
	