import serial.tools.list_ports
import csv
import time
import sys, threading
import numpy as np

from bebop.sensors.bebop_sensor_device_listener import Bebop_Sensor_Device_Listener
from bebop.sensors.device_descriptions.mini_testcard_remote import mini_testcard_remote_descriptor as remote_descriptor
from bebop.sensors.device_descriptions.mini_testcard_main import mini_testcard_main_descriptor as main_descriptor


class main_listener(threading.Thread):
    
    def __init__(self):
        threading.Thread.__init__(self)
        self.midi_array = [0, 0]
        
    def close(self):
        self.running = False
        
    def run(self):
        self.running = True
        
        main_listener_thread = Bebop_Sensor_Device_Listener(main_descriptor)
        main_listener_thread.start() 
    
        #global midi_array
        self.midi_array = main_listener_thread.return_pointer_to_array()
        
        while self.running is True:
            time.sleep(0.01)
            
            #if midi_array is not None:
            #print(main_midi_array)
            
class remote_listener(threading.Thread):
    
    def __init__(self):
        threading.Thread.__init__(self)
        self.midi_array = [0, 0]
        
    def close(self):
        self.running = False
        
    def run(self):
        self.running = True
        
        remote_listener_thread = Bebop_Sensor_Device_Listener(remote_descriptor)
        remote_listener_thread.start()
    
        #global midi_array
        self.midi_array = remote_listener_thread.return_pointer_to_array()
        
        while self.running is True:
            time.sleep(0.01)
            
            #if midi_array is not None:
            #print(remote_midi_array)
            #print('hoorayyyyyy\n')
            
class dual_board_listener(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.midi = [0,0,0,0]            ## Main board on bottom: read lines right to left
        
    def close(self):
        self.running = False
        
    def run(self):
        self.running = True
        
        main_thread = main_listener() 
        remote_thread = remote_listener()
        
        main_thread.start()
        time.sleep(1)
        remote_thread.start()
        time.sleep(1)        
        
        while self.running:
            self.midi = [main_thread.midi_array[0], remote_thread.midi_array[1],remote_thread.midi_array[0], main_thread.midi_array[1]]
            time.sleep(0.1)
                

if __name__ == "__main__":
    midi_thread = dual_board_listener()
    midi_thread.start()
    time.sleep(2)
    
    print('Waiting for MIDI Data.', end="", flush=True)
    while(midi_thread.midi[0] == 0 and midi_thread.midi[1] == 0 and midi_thread.midi[2] == 0 and midi_thread.midi[3] == 0):
        print('.', end="", flush=True)
        time.sleep(.5)
    
    while(True): 
        print(midi_thread.midi)
        time.sleep(0.25)
    
    
    #record_thread.close()