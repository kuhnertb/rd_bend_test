#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 19:25:14 2016
@author: mcanulty
"""

import threading
import time, sys
import queue
import mido
import mido.backends.rtmidi  
import signal
import numpy as np
from . seatDefines import sensorIndexToNames,  sensorNameToIndex


mido.set_backend('mido.backends.rtmidi')



#==============================================================================
#==============================================================================

sensorArray = np.array(list(range(84)))

class MidiQuery(threading.Thread):
    """ 
        Thread:
        Query for midi and enqueue data with a timestamp for main to process
    """


    #*****************
    #***  Init  ******
    #*****************
    def __init__ (self, queue, deviceName):
        threading.Thread.__init__(self)
        self.queue = queue
        self.arr  = sensorArray
        self.deviceName = deviceName
        self.sensorNum = 0
    #*****************
    #***  Close  *****
    #*****************        
    def close(self):
        self.running = False

    #*****************
    #***  Run ********
    #*****************     
    def run(self):
        self.running = True
        inputs = mido.get_input_names()

        print(("\n\n\n" , "Checking midi ports..."))
        print(("Available inputs are:", inputs))



        if self.deviceName in inputs:
            midiLoop = True

            print(("\n", self.deviceName, " available, opening port..."))
            sys.stdout.flush()              # Have to flush stdout here, or else
                                            # the output gets held up until later
            inport = mido.open_input(self.deviceName)
        else:
            midiLoop = False
            inport = None

        #**************************
        #**** MAIN THREAD LOOP  ***
        #**************************
        x = 0
        y = 0
        adc = 0
        #output = mido.open_output('SeatSensor_Middleware', virtual=True)
        
        while midiLoop is True:

            time.sleep(0.1)
            for msg in inport.iter_pending():  #without iter_pending, this blocks like a while loop
                #self.queue.put_nowait(msg)
                #print msg
                if(msg.channel == 15):
                    senseNum = msg.value ;
                    driveNum = msg.control;
                else:
                    adc = (     ((msg.channel & 0x7F) << 14)
                            |   ((msg.control & 0x7F) << 7)
                            |    (msg.value & 0x7F))
                    
                    sensorName = str(senseNum) + '_' + str(driveNum)
                    
                    self.sensorNum = sensorNameToIndex[sensorName]
                    
                    #print sensorNum, adc
                    self.arr[self.sensorNum] = adc
                    self.queue.put_nowait(1)


            if self.running is False:
                midiLoop = False
        #**************************
        #**** CLEANUP  ************
        #**************************
        if inport != None:
            inport.close()
            print((self.deviceName + ': Midi port closed'))
        else:
            print("No device to close")

#==============================================================================
#==============================================================================


def checkForMidi(midiQueue):
    try:
        msg = midiQueue.get_nowait()
    except queue.Empty:
        return None
    else:
        return sensorArray





#==============================================================================
#==============================================================================

running = True

def signal_handler(signal, frame):      
    global running                  # This catches Ctrl+C  and allows the program to properly      
    running = False                 # close itself, ideally preventing any attached devices 
                                    # from being unintentionally left open and inaccessible.
    print("SIGINT")

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
#==============================================================================
#==============================================================================        

midi_thread = None    


def main():


    #***************************************
    #****  SET UP QUEUES AND THREADS  ******
    #***************************************    
    global midi_thread
    deviceName = 'SeatSensor'
    midiQueue = queue.Queue()
    midi_thread = MidiQuery(midiQueue, deviceName)
    midi_thread.start()

    time.sleep(2)
    count = 0
    while running:
        d = checkForMidi(midiQueue)

        if d is not None:
            #print d
            print(count)
            count += 1
            
            
    midi_thread.close()

    time.sleep(1)


if __name__ == '__main__':
    main()

