#!/usr/bin/env python
#coding:utf-8
"""
Created on 07/26/18
@author:  mcanulty
"""
from bebop.sensors.bebop_sensor_device_listener import Bebop_Sensor_Device_Listener
from bebop.sensors.device_descriptions.resistor_divider_drt import device_descriptor
import time, threading
import numpy as np



'''
Voltage divider equations:  
  
  Vout = Vin * (R2 / (R1 + R2))
  R1 = R2 * ((Vin/Vout) - 1)			   //Solve for pullup
  R2 = R1 * (1 / ((Vin/Vout) - 1))	       //Solve for pulldown
  
ADC conversion:

  (ADC_resolution / VREF) = (ADC_reading / Vmeasured)
  Vmeasured = (VREF/ADC_resolution) * ADC_reading  
'''



class Resistor_Divider_Sensor_Listener(threading.Thread):
    def __init__ (self, r2=1000, verbose=False, infinity = float('Inf'), dtype=float):
        threading.Thread.__init__(self)
        self.listener_thread = Bebop_Sensor_Device_Listener(device_descriptor, sniff=False)
        self.running = False     
        
        self.vin = 1024
        self.vout = 0
        self.r2 = r2
        
        self.device_data = np.zeros(5, dtype=dtype)
        self.verbose = verbose
        self.infinity = infinity
        
    def run(self):
        self.listener_thread.start()
        self.adc_data_array =  self.listener_thread.return_pointer_to_array()
        self.running = True
        self.old_r1 = 0
        self.r1 = self.infinity
        self.adc_data_array[0] = 0
        self.adc_data_array[1] = 0
        
        
        
        
        while self.running:
            time.sleep(0.01)
            self.vin = self.adc_data_array[2]
            self.vout = self.adc_data_array[1]
            self.loadcell =  self.adc_data_array[0]
            if self.vout > 0:
                self.r1 = self.r2 * ((self.vin/self.vout) - 1)
            else:
                self.r1 = self.infinity

            if self.verbose:    
                if self.old_r1 != self.r1:
                    self.old_r1 = self.r1
                    print(self.r1)
            
            self.device_data[0] = self.r1
            self.device_data[1] = self.r2
            self.device_data[2] = self.vin
            self.device_data[3] = self.vout
            self.device_data[4] = self.loadcell
            
        
        #end thread 
        self.listener_thread.close()
        
        
    def close(self):
        self.running = False
        


if __name__ == "__main__":
    
    
    
    div = Resistor_Divider_Sensor_Listener()
    div.start()
    
    while True:
        time.sleep(0.2)
        print(div.device_data[2:])
    














