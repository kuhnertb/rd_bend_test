#!/usr/bin/env python
#coding:utf-8
"""
Created on 07/16/18
@author:  mcanulty
"""
import time, bisect, threading
from bebop.sensors.bebop_sensor_device_listener import Bebop_Sensor_Device_Listener
from bebop.sensors.device_descriptions.fabric_sensor_one import device_descriptor
from Notebook_Library.ram import calc_resistance_array

import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import numpy as np




pot_vals = [10, 392, 784, 1176, 1569, 1961, 2353, 2745, 3137, 3529, 3922, 4314, 4706, 5098, 5490, 5882, 6275, 6667, 7059, 7451, 7843, 8235, 8627, 9020, 9412, 9804, 10196, 10588, 10980, 11373, 11765, 12157, 12549, 12941, 13333, 13725, 14118, 14510, 14902, 15294, 15686, 16078, 16471, 16863, 17255, 17647, 18039, 18431, 18824, 19216, 19608, 20000, 20392, 20784, 21176, 21569, 21961, 22353, 22745, 23137, 23529, 23922, 24314, 24706, 25098, 25490, 25882, 26275, 26667, 27059, 27451, 27843, 28235, 28627, 29020, 29412, 29804, 30196, 30588, 30980, 31373, 31765, 32157, 32549, 32941, 33333, 33725, 34118, 34510, 34902, 35294, 35686, 36078, 36471, 36863, 37255, 37647, 38039, 38431, 38824, 39216, 39608, 40000, 40392, 40784, 41176, 41569, 41961, 42353, 42745, 43137, 43529, 43922, 44314, 44706, 45098, 45490, 45882, 46275, 46667, 47059, 47451, 47843, 48235, 48627, 49020, 49412, 49804, 50196, 50588, 50980, 51373, 51765, 52157, 52549, 52941, 53333, 53725, 54118, 54510, 54902, 55294, 55686, 56078, 56471, 56863, 57255, 57647, 58039, 58431, 58824, 59216, 59608, 60000, 60392, 60784, 61176, 61569, 61961, 62353, 62745, 63137, 63529, 63922, 64314, 64706, 65098, 65490, 65882, 66275, 66667, 67059, 67451, 67843, 68235, 68627, 69020, 69412, 69804, 70196, 70588, 70980, 71373, 71765, 72157, 72549, 72941, 73333, 73725, 74118, 74510, 74902, 75294, 75686, 76078, 76471, 76863, 77255, 77647, 78039, 78431, 78824, 79216, 79608, 80000, 80392, 80784, 81176, 81569, 81961, 82353, 82745, 83137, 83529, 83922, 84314, 84706, 85098, 85490, 85882, 86275, 86667, 87059, 87451, 87843, 88235, 88627, 89020, 89412, 89804, 90196, 90588, 90980, 91373, 91765, 92157, 92549, 92941, 93333, 93725, 94118, 94510, 94902, 95294, 95686, 96078, 96471, 96863, 97255, 97647, 98039, 98431, 98824, 99216, 99608, 100000]


#*******************************************************************************
#*******************************************************************************
#*******************************************************************************

class Fabric_Sensor_Listener(threading.Thread):
    def __init__ (self):
        threading.Thread.__init__(self)
        self.running = False
        self.listener_thread = Bebop_Sensor_Device_Listener(device_descriptor)
        self.data_ready_mutex = threading.Lock()
        self.digipot = 127
        self.last_digipot = 0
        self.xy_ohm_array = np.zeros(shape=(16, 16), dtype="float")
        self.digipot_ohms = 0
        self.frames = []
        
    def run(self):
        self.running = True
        self.listener_thread.start()
        
        
        time.sleep(0.25)
        
        self.listener_thread.xy_array =  np.zeros(shape=(16, 16), dtype="float")  #replace listener array with floats for calcs
        self.xy_adc_array = self.listener_thread.xy_array
        self.listener_thread.change_encoding('fabric_sensor_16_bit')
        self.listener_thread.program_change(2)                                    #this switches to higher speed
        self.listener_thread.send_8bit_cc(self.digipot)
        self.mean_ohm = 0
        
        
        
        while self.running:
            
            #wait for adc to update
            time.sleep(0.1)
            
            #Calculate_data
            self.data_ready_mutex.acquire()
            self.xy_ohm_array = calc_resistance_array(self.xy_adc_array, self.digipot_ohms, 1024)
            self.last_digipot = self.digipot_ohms
            self.data_ready_mutex.release()
            
            #calc new digipot
            self.mean_ohm = self.xy_ohm_array[2:14, 2:14].mean()
            self.digipot_calc = bisect.bisect_right(pot_vals, self.mean_ohm) - 1
            
            #update digipot
            self.listener_thread.send_8bit_cc(self.digipot_calc)
            self.digipot = self.digipot_calc
            self.digipot_ohms = pot_vals[self.digipot]
            #self.frames.append(self.xy_ohm_array.copy())
        
        self.listener_thread.close()
    def close(self):
        self.running = False
        
        
    def return_ohm_data(self):
        self.data_ready_mutex.acquire()
        out_tuple = (self.last_digipot, self.xy_ohm_array.copy())
        self.data_ready_mutex.release()
        return out_tuple

    def clear_frames(self):
        self.frames = []


#*******************************************************************************
#*******************************************************************************
#*******************************************************************************


def updatefig(*args):
    global fab_sensor
    
    digipot, ohm_array = fab_sensor.return_ohm_data()
    data_array = fab_sensor.xy_adc_array
    print(digipot, fab_sensor.mean_ohm, data_array.mean())
    
    #np.savetxt(str(digipot) + '_adc.txt', data_array, fmt='%10.0f)', delimiter=' ', newline='\n', header='', 
              #footer='', comments='# ', encoding=None)
    #np.savetxt(str(digipot) + '_ohm.txt', ohm_array, fmt='%10.0f)', delimiter=' ', newline='\n', header='', 
              #footer='', comments='# ', encoding=None)    
    im.set_array(data_array)
    return im,


if __name__ == '__main__':

    global fab_sensor
    
    
    #*************  SELF CALIBRATING FABRIC SENSOR  **********************
    fab_sensor = Fabric_Sensor_Listener()
    fab_sensor.start()
    time.sleep(0.25)
    digipot, data_array = fab_sensor.return_ohm_data() 
    #*************  SELF CALIBRATING FABRIC SENSOR  **********************
    
    fig = plt.figure()
    im = plt.imshow(data_array, animated=True, vmin=0, vmax=1024, cmap='viridis_r')
    ani = animation.FuncAnimation(fig, updatefig, interval=100, blit=True)
    plt.show()    

    fab_sensor.close()
    
    
















