#!/usr/bin/env python
#coding:utf-8

from bebop.sensors.bebop_sensor_device import Bebop_Sensor_Device_Descriptor
from midiSensor.sensor_define_generators import alicat_csv_titles_with_metric, gen_sensor_names_with_sense_drive

array_of_indexed_sensor_names = ['s0_d0', 's1_d0', 's2_d0']

num_sensors = len(array_of_indexed_sensor_names)


divider_device_dict = {
    'midi_device_name': 'Resistor_Divider', 
    'num_sensors': num_sensors,
    'array_of_indexed_sensor_names': array_of_indexed_sensor_names, 
    'leftmost_csv_header_string': alicat_csv_titles_with_metric, 
    'device_type' : 'bebop_midi_device', 
    'data_encoding' : 'bebop_midi_xy_int'    
}


device_descriptor = Bebop_Sensor_Device_Descriptor(divider_device_dict)


if __name__ == "__main__":

    print(device_descriptor.csv_header, end='')
    print(device_descriptor.first_sensor_column_in_csv)
    print(device_descriptor.dict_of_sensor_name_to_index)
    print(array_of_indexed_sensor_names)
    
    
